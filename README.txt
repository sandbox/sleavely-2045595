This module adds new configuration options for the user login form:

- Ability to tighten the floodcontrol that Drupal Core uses for that form
- Ability to set the autocomplete="off" attribute
- Ability to turn on whitelisting of IP addresses, with octet wildcards

The settings page appears at:
Configuration -> People -> Enhanced login security
